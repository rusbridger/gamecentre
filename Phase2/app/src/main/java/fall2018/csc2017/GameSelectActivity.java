package fall2018.csc2017;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import fall2018.csc2017.R;

import fall2018.csc2017.minesweeper.MinesweeperStartingActivity;
import fall2018.csc2017.pipes.PipesStartingActivity;
import fall2018.csc2017.slidingtiles.SlidingTilesStartingActivity;

public class GameSelectActivity extends AppCompatActivity {
    /**
     * Init the activity and create the buttons.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game_select);
        addMinesweeperButtonListener();
        addPipesTilesButtonListener();
        addSlidingTilesButtonListener();
    }

    /**
     * Create Button to Choose the SlidingTiles Game
     */
    private void addSlidingTilesButtonListener() {
        Button slidingTilesButton = findViewById(R.id.SlidingTilesSelectButton);
        slidingTilesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent slidingTilesIntent = new Intent(GameSelectActivity.this, SlidingTilesStartingActivity.class);
                GameSelectActivity.this.startActivity(slidingTilesIntent);
            }
        });
    }

    /**
     * Create Button to Choose the Pipes Game
     */
    private void addPipesTilesButtonListener() {
        Button pipesTilesButton = findViewById(R.id.PipesSelectButton);
        pipesTilesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pipesTilesIntent = new Intent(GameSelectActivity.this, PipesStartingActivity.class);
                GameSelectActivity.this.startActivity(pipesTilesIntent);
            }
        });
    }

    /**
     * Create Button to Choose the Minesweeper Game
     */
    private void addMinesweeperButtonListener() {
        Button minesweeperButton = findViewById(R.id.MinesweeperSelectButton);
        minesweeperButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent minesweeperIntent = new Intent(GameSelectActivity.this, MinesweeperStartingActivity.class);
                GameSelectActivity.this.startActivity(minesweeperIntent);
            }
        });
    }
}
