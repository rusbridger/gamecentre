package fall2018.csc2017.db;

import android.app.Application;
import android.content.Context;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(application = Application.class)
public class ScoreControllerTest {

    final static Context context = RuntimeEnvironment.application;

    @Before
    public void setUp() throws Exception {
        // We need to have the database ready
        if (DatabaseController.provideDatabase() == null) {
            DatabaseController.init(context.getApplicationContext());
        }
        UserController.createUser("epsilon", "mS");
        UserController.createUser("delta", "Sm");

        ScoreController.createScore("epsilon", Games.SLIDING_TILES.getValue(), 10, 20);
        ScoreController.createScore("epsilon", Games.SLIDING_TILES.getValue(), 20, 10);
        ScoreController.createScore("delta", Games.SLIDING_TILES.getValue(), 5, 75);
        ScoreController.createScore("delta", Games.SLIDING_TILES.getValue(), 35, 5);
        ScoreController.createScore("delta", Games.SLIDING_TILES.getValue(), 50, 0);

        ScoreController.createScore("epsilon", Games.MINESWEEPER.getValue(), 104, 240);
        ScoreController.createScore("epsilon", Games.MINESWEEPER.getValue(), 140, 204);
        ScoreController.createScore("delta", Games.MINESWEEPER.getValue(), 5, 533);
        ScoreController.createScore("delta", Games.MINESWEEPER.getValue(), 5000, 0);

        ScoreController.createScore("delta", Games.PIPES.getValue(), 100000, 0);
    }

    @After
    public void tearDown() {
        DatabaseController.reset();
        DatabaseController.kill();
    }

    @Test
    public void testSortByScoresIndiv() {
        // we more the timeTaken/scores unique since it's rather difficult to track score IDs
        Score[] scores = ScoreController.sortByScore(Games.SLIDING_TILES.getValue(), "epsilon");
        assertEquals(scores.length, 2);
        long[] actual = new long[2];
        long[] expected = {10L, 20L};
        for (int i = 0; i < scores.length; i++) {
            actual[i] = scores[i].getTimeTaken();
        }
        assertArrayEquals(expected, actual);
    }

    @Test
    public void testSortByTimeTakenIndiv() {
        // we more the timeTaken/scores unique since it's rather difficult to track score IDs
        Score[] scores = ScoreController.sortByTimeTaken(Games.SLIDING_TILES.getValue(), "epsilon");
        assertEquals(scores.length, 2);
        int[] actual = new int[2];
        int[] expected = {20, 10};
        for (int i = 0; i < scores.length; i++) {
            actual[i] = scores[i].getScore();
        }
        assertArrayEquals(expected, actual);
    }

    @Test
    public void testSortByScores() {
        Score[] scores = ScoreController.sortByScore(Games.SLIDING_TILES.getValue());
        assertEquals(scores.length, 5);
        long[] actualTimeTaken = new long[5];
        long[] expectedTimeTaken = {5L, 10L, 20L, 35L, 50L};
        String[] expectedOwners = {"delta", "epsilon", "epsilon", "delta", "delta"};

        for (int i = 0; i < 5; i++) {
            assertTrue(scores[i].getUsername().equals(expectedOwners[i]));
            actualTimeTaken[i] = scores[i].getTimeTaken();
        }
        assertArrayEquals(expectedTimeTaken, actualTimeTaken);
    }

    @Test
    public void testSortByTimeTaken() {
        Score[] scores = ScoreController.sortByTimeTaken(Games.SLIDING_TILES.getValue());
        assertEquals(scores.length, 5);
        int[] actualScores = new int[5];
        int[] expectedScores = {75, 20, 10, 5, 0};
        String[] expectedOwners = {"delta", "epsilon", "epsilon", "delta", "delta"};
        for (int i = 0; i < 5; i++) {
            assertTrue(scores[i].getUsername().equals(expectedOwners[i]));
            actualScores[i] = scores[i].getScore();
        }
        assertArrayEquals(expectedScores, actualScores);
    }

    @Test
    public void testEqualsAndToString() {
        ScoreController.createScore("epsilon", 5, 0, 5);
        ScoreController.createScore("epsilon", 5, 0, 5);
        ScoreController.createScore("epsilon", 5, 1, 4);
        Score[] scores = ScoreController.sortByTimeTaken(5);
        assertTrue(scores[0].toString() instanceof String);
        assertFalse(scores[0].equals(scores[1]));
        assertFalse(scores[0].equals(scores[2]));
    }

}