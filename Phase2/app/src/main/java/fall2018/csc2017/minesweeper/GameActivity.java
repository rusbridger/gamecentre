package fall2018.csc2017.minesweeper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Observable;
import java.util.Observer;

import fall2018.csc2017.CustomAdapter;
import fall2018.csc2017.db.Games;
import fall2018.csc2017.db.Gamesave;
import fall2018.csc2017.db.GamesaveController;
import fall2018.csc2017.db.Score;
import fall2018.csc2017.db.ScoreController;
import fall2018.csc2017.db.User;
import fall2018.csc2017.R;

public class GameActivity extends AppCompatActivity implements Observer {
    /**
     * The grid view and calculated column height and width based on device size.
     */
    private GestureDetectGridView gridView;
    private int columnWidth, columnHeight;

    /**
     * The BoardManager of the current game.
     */
    private BoardManager boardManager;

    /**
     * The list of buttons in the board.
     */
    private ArrayList<Button> tileButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boardManager = (BoardManager) getIntent().getSerializableExtra("boardManager");
        boardManager.addObserver(this);

        setContentView(R.layout.activity_minesweeper_main);
        createTileButtons();

        setupGridView();

        startInfoUpdateThread();
    }

    @Override
    public void update(Observable o, Object arg) {
        display();
        updateRemainingBombsText();
        updateScoreText();

        if (boardManager.computeSolved()) {
            winGame();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            saveGame();
        } catch (IOException e) {
            Log.e("slidingtiles", "Error during autosave", e);
        }
    }

    /**
     * Starts a thread to update info on time spent and score.
     */
    private void startInfoUpdateThread() {
        // Start a new thread
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        // Update once every second
                        Thread.sleep(1000);
                        runOnUiThread(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!boardManager.isGameOver()) {
                                            TextView time = findViewById(R.id.TimerText);
                                            long newGameTime = boardManager.getGameTime() + 1;
                                            boardManager.setGameTime(newGameTime);

                                            time.setText("TIME: " + newGameTime);
                                        }
                                    }
                                }
                        );
                    }
                } catch (InterruptedException e) {

                }
            }
        };

        thread.start();
    }

    /**
     * Create the tile buttons.
     */
    private void createTileButtons() {
        MinesweeperBoard minesweeperBoard = boardManager.getMinesweeperBoard();
        tileButtons = new ArrayList<>();
        for (int row = 0; row < minesweeperBoard.getNumRows(); row++) {
            for (int col = 0; col < minesweeperBoard.getNumColumns(); col++) {
                Button button = new Button(this);
                button.setBackgroundResource(minesweeperBoard.getTile(row, col).getBackground());
                tileButtons.add(button);
            }
        }
    }

    /**
     * Setup the grid view.
     */
    private void setupGridView() {
        gridView = findViewById(R.id.grid);
        gridView.setNumColumns(boardManager.getMinesweeperBoard().getNumColumns());
        gridView.setBoardManager(boardManager);
        gridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        gridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int displayWidth = gridView.getMeasuredWidth();
                        int displayHeight = gridView.getMeasuredHeight();

                        columnWidth = displayWidth / boardManager.getMinesweeperBoard().getNumColumns();
                        columnHeight = displayHeight / boardManager.getMinesweeperBoard().getNumRows();

                        display();
                    }
                }
        );
    }

    /**
     * Displays the tile buttons.
     */
    public void display() {
        updateTileButtons();
        gridView.setAdapter(new CustomAdapter(tileButtons, columnWidth, columnHeight));
    }

    /**
     * Update the tile buttons to new backgrounds.
     */
    private void updateTileButtons() {
        MinesweeperBoard minesweeperBoard = boardManager.getMinesweeperBoard();
        int nextPos = 0;
        for (Button b : tileButtons) {
            int row = nextPos / minesweeperBoard.getNumRows();
            int col = nextPos % minesweeperBoard.getNumColumns();
            b.setBackgroundResource(minesweeperBoard.getTile(row, col).getBackground());
            nextPos++;
        }
    }

    /**
     * Update the text view to show the remaining number of bombs.
     */
    private void updateRemainingBombsText() {
        TextView remainingBombsText = findViewById(R.id.RemainingBombsText);
        remainingBombsText.setText("BOMBS: " +
                (boardManager.getNumBombs() - boardManager.getNumBombsMarked()));
    }

    /**
     * Update the text view to show the score.
     */
    private void updateScoreText() {
        TextView scoreText = findViewById(R.id.ScoreText);
        scoreText.setText("SCORE: " + boardManager.getScore());
    }

    /**
     * Saves the current game state to the database.
     *
     * @throws IOException
     */
    private void saveGame() throws IOException {
        GamesaveController.createGamesave(User.getSignedInUser().getUsername(),
                Games.MINESWEEPER.getValue(), boardManager);
    }


    /**
     * Stores the score to the database when winning.
     */
    private void winGame() {
        ScoreController.createScore(User.getSignedInUser().getUsername(),
                Games.MINESWEEPER.getValue(), (long) (boardManager.getGameTime() * 1e9),
                boardManager.getScore());
    }
}
