package fall2018.csc2017.pipes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BoardManagerTest {
    private BoardManager bm0, bm1, bmc;

    @Before
    public void setup() {
        bm0 = new BoardManager(0);
        bm1 = new BoardManager();
        bm0.touchRotate(4, true);
    }

    @Test
    public void testGetScore() {
        assertEquals(3, bm1.getScore());
    }

    @Test
    public void testLifeTime() {
        bm1.setLifetime(100);
        assertEquals(100, bm1.getLifetime());
    }

    @Test
    public void testPuzzleSolved() {
        boolean solved = bm1.puzzleSolved();
        while (!solved) {
            bm1.touchRotate(4, false);
            solved = bm1.puzzleSolved();
        }
        assertTrue(solved);
    }

    @Test
    public void guaranteedRotate() {
        assertTrue(bm0.isValidTap(4));
    }

    @Test
    public void testDudSolved() {
        boolean solved = bm0.puzzleSolved();
        assertFalse(solved);
    }

    @Test
    public void testDudUndo() {
        while (bm0.undo()) {
            bm0.undo();
        }
        assertFalse(bm0.undo());
    }

    @Test
    public void makeCopyOfBoard() {
        PipesGameBoard copy = bm1.getPipesGameBoard();
        BoardManager bmc = new BoardManager(copy);
        boolean same = bm1.getPipesGameBoard().getTile(4).getType() == bmc.getPipesGameBoard().getTile(4).getType();
        System.out.println(same);
        assertTrue(same);
    }
}