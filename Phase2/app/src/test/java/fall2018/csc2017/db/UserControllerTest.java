package fall2018.csc2017.db;

import android.content.Context;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
public class UserControllerTest {

    final static Context context = RuntimeEnvironment.application;

    @Before
    public void setUp() throws Exception {
        DatabaseController.init(context.getApplicationContext());
    }

    @After
    public void tearDown() throws Exception {
        DatabaseController.reset();
        DatabaseController.kill();
    }

    @Test
    public void testUserWorks() {
        UserController.createUser("epsilon", "delta");
        assertNotNull(UserController.authenticateUser("epsilon", "delta"));
        // excellent password practices
        assertNull(UserController.authenticateUser("epsilon", "password"));
    }

    @Test
    public void ensureConflictError() {
        UserController.createUser("eps", "nev");
        boolean pass = true;
        try {
            UserController.createUser("eps", "eve");
            // shouldn't get here!
            pass = false;
        } catch (Exception e) {
        }
        try {
            // same password
            UserController.createUser("eps", "nev");
            // shouldn't get here!
            pass = false;
        } catch (Exception e) {
        }
        assertTrue(pass);
    }
}