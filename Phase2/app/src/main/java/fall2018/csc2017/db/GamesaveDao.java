package fall2018.csc2017.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

/**
 * The DAO interface for game saves.
 */
@Dao
public interface GamesaveDao {
    /**
     * Saves a game, replaces the previous entry in the database if one exists.
     *
     * @param gamesave the game save to save.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void createGamesave(Gamesave gamesave);

    /**
     * Gets the saved game from the database.
     *
     * @param username username of the user this game belongs to
     * @param gameId   game ID
     * @return the saved game if one exists, null otherwise
     */
    @Query("SELECT * FROM gamesave WHERE username = :username AND gameId = :gameId LIMIT 1")
    Gamesave getGamesave(String username, int gameId);
}
