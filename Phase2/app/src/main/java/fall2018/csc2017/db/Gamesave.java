package fall2018.csc2017.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Model for a game save.
 */
@Entity(foreignKeys = @ForeignKey(entity = User.class, parentColumns = "username", childColumns = "username"),
        indices = {@Index(value = {"username", "gameId"}, unique = true)})
public class Gamesave {
    /**
     * Primary key of the database entry.
     */
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    /**
     * Foreign key of the username of the user this game save belongs to.
     */
    @NonNull
    private String username;

    /**
     * Game ID corresponding to the game in the Games enum.
     */
    @NonNull
    private int gameId;

    /**
     * The BLOB of the saved game.
     */
    @NonNull
    private String serializeBlob;

    public Gamesave() {
    }

    public Gamesave(String username, int gameId, String serializeBlob) {
        this.username = username;
        this.gameId = gameId;
        this.serializeBlob = serializeBlob;
    }

    /**
     * Getter for the ID of this game save.
     *
     * @return the ID of this game save
     */
    @NonNull
    public int getId() {
        return id;
    }

    /**
     * Setter for the ID of this game save.
     *
     * @param id the ID to set this game save to
     */
    public void setId(@NonNull int id) {
        this.id = id;
    }

    /**
     * Getter for the username that this game save belongs to.
     *
     * @return the username that this game save belongs to
     */
    @NonNull
    public String getUsername() {
        return username;
    }

    /**
     * Setter for the username that this game save belongs to.
     *
     * @param username the username that this game save belongs to
     */
    public void setUsername(@NonNull String username) {
        this.username = username;
    }

    /**
     * Getter for the game ID.
     *
     * @return the game ID
     */
    @NonNull
    public int getGameId() {
        return gameId;
    }

    /**
     * Setter for the game ID.
     *
     * @param gameId the game ID
     */
    public void setGameId(@NonNull int gameId) {
        this.gameId = gameId;
    }

    /**
     * Getter for the serialized BLOB of the saved game.
     *
     * @return serialized BLOB of the saved game
     */
    @NonNull
    public String getSerializeBlob() {
        return serializeBlob;
    }

    /**
     * Setter for the serialized BLOB of the saved game.
     *
     * @param serializeBlob serialized BLOB of the saved game
     */
    public void setSerializeBlob(@NonNull String serializeBlob) {
        this.serializeBlob = serializeBlob;
    }
}
