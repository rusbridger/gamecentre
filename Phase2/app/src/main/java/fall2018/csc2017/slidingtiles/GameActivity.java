package fall2018.csc2017.slidingtiles;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import fall2018.csc2017.CustomAdapter;
import fall2018.csc2017.R;
import fall2018.csc2017.db.Games;
import fall2018.csc2017.db.GamesaveController;
import fall2018.csc2017.db.ScoreController;
import fall2018.csc2017.db.User;

/**
 * The game activity.
 */
public class GameActivity extends AppCompatActivity implements Observer {
    private static int columnWidth, columnHeight;

    private BoardManager boardManager;

    /**
     * The buttons to display.
     */
    private ArrayList<Button> tileButtons;
    // Grid View
    private GestureDetectGridView gridView;
    private long startup_time;

    /**
     * Set up the background image for each button based on the master list
     * of positions, and then call the adapter to set the view.
     */
    // Display
    public void display() {
        updateTileButtons();
        gridView.setAdapter(new CustomAdapter(tileButtons, columnWidth, columnHeight));
    }

    /*
     * When we create the game, we want to load what board state was passed in from the starting
     * activity. This way, we can eliminate the static board.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boardManager = (BoardManager) getIntent().getSerializableExtra("boardManager");
        if (boardManager == null) {
            boardManager = new BoardManager();
        }

        createTileButtons(this);
        setContentView(R.layout.activity_slidingtiles_main);


        // Add View to activity
        gridView = findViewById(R.id.grid);
        final int num_rows = boardManager.getSlidingTilesBoard().numRows;
        final int num_cols = boardManager.getSlidingTilesBoard().numColumns;
        gridView.setNumColumns(num_cols);
        gridView.setBoardManager(boardManager);
        boardManager.getSlidingTilesBoard().addObserver(this);
        // Observer sets up desired dimensions as well as calls our display function
        gridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        gridView.getViewTreeObserver().removeOnGlobalLayoutListener(
                                this);
                        int displayWidth = gridView.getMeasuredWidth();
                        int displayHeight = gridView.getMeasuredHeight();

                        int displaySize = Math.min(displayHeight, displayWidth);

                        columnWidth = displaySize / num_cols;
                        columnHeight = displaySize / num_rows;

                        display();
                    }
                });
        addUndoButtonListener();
        startup_time = System.nanoTime();

        startInfoUpdateThread();
    }

    /**
     * Starts a thread to update info on time spent and score.
     */
    private void startInfoUpdateThread() {
        // Start a new thread
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        // Update once every second
                        Thread.sleep(1000);
                        runOnUiThread(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        // Update the fields
                                        TextView time = findViewById(R.id.TimerText);
                                        long timeChanged = (long) (
                                                (boardManager.getLifetime() + System.nanoTime()
                                                        - startup_time) * 1e-9);
                                        time.setText("TIME: " + timeChanged);

                                        TextView moves = findViewById(R.id.ScoreText);
                                        moves.setText("SCORE: " + boardManager.getScore());
                                    }
                                });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        thread.start();
    }

    /**
     * Create the buttons for displaying the tiles.
     *
     * @param context the context
     */
    private void createTileButtons(Context context) {
        SlidingTilesBoard slidingTilesBoard = boardManager.getSlidingTilesBoard();
        tileButtons = new ArrayList<>();
        for (int row = 0; row != slidingTilesBoard.getNumRows(); row++) {
            for (int col = 0; col != slidingTilesBoard.getNumColumns(); col++) {
                Button tmp = new Button(context);
                tmp.setBackgroundResource(slidingTilesBoard.getTile(row, col).getBackground());
                this.tileButtons.add(tmp);
            }
        }
    }

    /**
     * Update the backgrounds on the buttons to match the tiles.
     */
    private void updateTileButtons() {
        SlidingTilesBoard slidingTilesBoard = boardManager.getSlidingTilesBoard();
        int nextPos = 0;
        for (Button b : tileButtons) {
            int row = nextPos / slidingTilesBoard.getNumRows();
            int col = nextPos % slidingTilesBoard.getNumColumns();
            b.setBackgroundResource(slidingTilesBoard.getTile(row, col).getBackground());
            nextPos++;
        }
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
        try {
            saveGame();
        } catch (IOException e) {
            Log.e("slidingtiles", "Error during autosave", e);
        }
    }
    /*
     * After something changes, we want to know if the user has won.
     */
    @Override
    public void update(Observable o, Object arg) {
        if (boardManager.puzzleSolved()) {
            winGame();
        }
        display();
    }

    /**
     * Saves the current game state to the database.
     *
     * @throws IOException
     */
    private void saveGame() throws IOException {
        long timeChanged = System.nanoTime() - startup_time;

        boardManager.setLifetime(boardManager
                .getLifetime() + timeChanged);
        GamesaveController.createGamesave(User.getSignedInUser().getUsername(),
                Games.SLIDING_TILES.getValue(), boardManager);
    }

    /**
     * Adds listener for UndoButton.
     */
    private void addUndoButtonListener() {
        Button undoButton = findViewById(R.id.UndoButton);
        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boardManager.puzzleSolved()) {
                    Toast.makeText(GameActivity.this, "You already won!",
                            Toast.LENGTH_SHORT).show();
                } else if (!gridView.undoButtonClick()) { // if the user ran out of undos
                    Toast.makeText(GameActivity.this, "No moves to undo ¯\\_(ツ)_/¯",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Saves the score, then exists the game.
     */
    private void winGame() {
        Toast.makeText(this.getApplicationContext(), "YOU WIN!", Toast.LENGTH_SHORT).show();
        long finalTime = System.nanoTime() + boardManager.getLifetime() - startup_time;
        ScoreController.createScore(User.getSignedInUser().getUsername(),
                Games.SLIDING_TILES.getValue(), finalTime, boardManager.getScore());
    }
}
