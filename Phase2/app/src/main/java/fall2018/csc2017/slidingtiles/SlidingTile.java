package fall2018.csc2017.slidingtiles;

import android.support.annotation.NonNull;

import fall2018.csc2017.AbstractTile;
import fall2018.csc2017.R;

/**
 * A MinesweeperTile in a sliding tiles puzzle.
 */
public class SlidingTile extends AbstractTile implements Comparable<SlidingTile> {

    /**
     * The list of images available
     */
    private static final int[] TILES = {
            R.drawable.slidingtiles_tile_01, R.drawable.slidingtiles_tile_02, R.drawable.slidingtiles_tile_03,
            R.drawable.slidingtiles_tile_04, R.drawable.slidingtiles_tile_05, R.drawable.slidingtiles_tile_06,
            R.drawable.slidingtiles_tile_07, R.drawable.slidingtiles_tile_08, R.drawable.slidingtiles_tile_09,
            R.drawable.slidingtiles_tile_10, R.drawable.slidingtiles_tile_11, R.drawable.slidingtiles_tile_12,
            R.drawable.slidingtiles_tile_13, R.drawable.slidingtiles_tile_14, R.drawable.slidingtiles_tile_15,
            R.drawable.slidingtiles_tile_16, R.drawable.slidingtiles_tile_17, R.drawable.slidingtiles_tile_18,
            R.drawable.slidingtiles_tile_19, R.drawable.slidingtiles_tile_20, R.drawable.slidingtiles_tile_21,
            R.drawable.slidingtiles_tile_22, R.drawable.slidingtiles_tile_23, R.drawable.slidingtiles_tile_24,
            R.drawable.slidingtiles_tile_blank
    };

//    /**
//     * The background id to find the tile image.
//     */
//    private int background;
//
//    /**
//     * The unique id.
//     */
//    private int id;
//
//    /**
//     * Return the background id.
//     *
//     * @return the background id
//     */
    public int getBackground() {
        return super.getBackground();
    }

    /**
     * Return the tile id.
     *
     * @return the tile id
     */
    public int getId() {
        return super.getId();
    }

    /**
     * A MinesweeperTile with id and background. The background may not have a corresponding image.
     *
     * @param id         the id
     * @param background the background
     */
    public SlidingTile(int id, int background) {
        super(id, background);
    }

    /**
     * A tile with a background id; look up and set the id.
     *
     * @param backgroundId the id of the background image
     * @param numRows     the number of rows on the board
     * @param numColumns     the number of columns on the board
     */
    public SlidingTile(int backgroundId, int numRows, int numColumns) {
        this.background = backgroundId;
        if (backgroundId + 1 == numRows * numColumns) {
            background = R.drawable.slidingtiles_tile_blank;
        } else {
            background = TILES[backgroundId];
        }

        id = backgroundId + 1;
    }

    @Override
    public int compareTo(@NonNull SlidingTile o) {
        return super.compareTo(o);
    }
}
