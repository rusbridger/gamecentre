package fall2018.csc2017.db;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Base64;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;


/**
 * Abstraction layer to avoid exposing the database to the rest of the app.
 */
public class GamesaveController {

    /**
     * Saves the object into the database under the specified username and game ID.
     * Does nothing if the save data is a null object.
     * Precondition: saveData is of the appropiate class.
     *
     * @param username the user to save the game to.
     * @param gameId   the game ID specified by the Games enum
     * @param saveData the object then represents the game's state.
     * @throws IOException
     */
    public static void createGamesave(String username, int gameId, Object saveData) throws IOException {
        if (saveData == null) {
            return;
        }
        createGamesave(username, gameId, serializeSaveToHex(saveData));
    }

    /**
     * Retrieves the object from the database under the specified username and game ID.
     * Precondition: the game state is based on the proper class. It is not the database's
     * responsibility to cast the save data.
     *
     * @param username the username to retrieve the game data from
     * @param gameId   the ID of the game
     * @return the uncasted object representation of the game save, null if it doesn't exist
     * @throws IOException
     */
    public static Object getGamesaveData(String username, int gameId) throws IOException, ClassNotFoundException {
        Gamesave data = getGamesave(username, gameId);
        if (data == null) {
            return null;
        }
        return decodeSerializedBoardManager(data.getSerializeBlob());
    }

    /*
     * Shortcut that creates the Gamesave before entry.
     */
    private static void createGamesave(String username, int gameId, String serializeBlob) {
        GamesaveDao gsDao = DatabaseController.provideDatabase().gamesaveDao();
        gsDao.createGamesave(new Gamesave(username, gameId, serializeBlob));
    }

    /*
     * Gets the saved game from the database.
     *
     * @param username username of the user this game belongs to
     * @param gameId   game ID
     * @return the saved game if one exists, null otherwise
     */
    private static Gamesave getGamesave(String username, int gameId) {
        GamesaveDao gsDao = DatabaseController.provideDatabase().gamesaveDao();
        return gsDao.getGamesave(username, gameId);
    }

    /*
     * Saves a serializable object into a hex string, then returns it.
     * The object and all of it's fields should be serializable.
     *
     * Use this to store the game save data as a string.
     * @param o the object to convert to a string.
     * @return the string rep of o, null if object is null
     * @throws IOException
     */
    private static String serializeSaveToHex(Object o) throws IOException {
        if (o == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(o);
        objectOutputStream.flush();

        byte[] bytes = byteArrayOutputStream.toByteArray();

        objectOutputStream.close();
        byteArrayOutputStream.close();

        return Base64.getEncoder().encodeToString(bytes);
    }

    /*
     * Converts a hexidecimal string to it's Object form. For this app we will always use this
     * on BoardManagers. It is not this class's responsibility to return the object in it's real class.
     * @param serStr the string to convert
     * @return the obj rep, null if serStr is null
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private static Object decodeSerializedBoardManager(String serStr) throws IOException, ClassNotFoundException {
        if (serStr == null) {
            return null;
        }
        byte[] bytes = Base64.getDecoder().decode(serStr);

        ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
        Object save = inputStream.readObject();

        inputStream.close();

        return save;
    }

}
