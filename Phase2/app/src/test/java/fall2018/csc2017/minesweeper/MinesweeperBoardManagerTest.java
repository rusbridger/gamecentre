package fall2018.csc2017.minesweeper;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MinesweeperBoardManagerTest {
    private BoardManager boardManager;

    @Before
    public void setup() {
        generateBoard();
    }

    @Test
    public void testTilesAreInitiallyHidden() {
        for (int r = 0; r < 4; r++) {
            for (int c = 0; c < 4; c++) {
                assertTrue(boardManager.getMinesweeperBoard().getTile(r, c).isHidden());
            }
        }
    }

    @Test
    public void testRevealTileGeneratesGameOnFirstTap() {
        boardManager = new BoardManager(4, 4, 4);
        boardManager.revealTile(0);

        MinesweeperTile tile = boardManager.getMinesweeperBoard().getTile(0, 0);

        assertNotEquals(9, tile.getTileVal());
        assertNotEquals(MinesweeperTile.HIDDEN_TILE, tile.getBackground());
    }

    @Test
    public void testRevealTileOnHiddenTile() {
        boardManager.revealTile(3);
        boardManager.revealTile(3);
        assertFalse(boardManager.getMinesweeperBoard().getTile(0, 3).isHidden());
    }

    @Test
    public void testRevealTileOnBombTile() {
        boardManager.revealTile(0);
        assertTrue(boardManager.isGameOver());
    }

    @Test
    public void testRevealTileDoesNotRevealWhenGameOver() {
        boardManager.revealTile(0);
        boardManager.revealTile(1);
        assertTrue(boardManager.getMinesweeperBoard().getTile(0, 1).isHidden());
    }

    @Test
    public void testMarkAsBombOnHiddenTile() {
        assertTrue(boardManager.markAsBomb(0));
        assertTrue(boardManager.getMinesweeperBoard().getTile(0, 0).isFlagged());
        assertTrue(boardManager.markAsBomb(0));
        assertFalse(boardManager.getMinesweeperBoard().getTile(0, 0).isFlagged());
    }

    @Test
    public void testCannotMarkAsBombWhenNoMoreBombsLeft() {
        boardManager.markAsBomb(0);
        boardManager.markAsBomb(1);
        boardManager.markAsBomb(2);
        boardManager.markAsBomb(3);
        assertFalse(boardManager.markAsBomb(4));
        assertFalse(boardManager.getMinesweeperBoard().getTile(1, 0).isFlagged());
    }

    @Test
    public void testBoardMarkedAsGameOver() {
        solveBoard();
        assertTrue(boardManager.isGameOver());
    }

    @Test
    public void testCannotMarkAsBombOnGameOver() {
        solveBoard();
        boardManager.markAsBomb(2);
        assertFalse(boardManager.getMinesweeperBoard().getTile(0, 2).isFlagged());
    }

    @Test
    public void testCannotRevealTileOnGameOver() {
        solveBoard();
        boardManager.revealTile(2);
        assertTrue(boardManager.getMinesweeperBoard().getTile(0, 2).isHidden());
    }

    @Test
    public void testCanMarkMoreBombs() {
        assertTrue(boardManager.canMarkMoreBombs());
        boardManager.markAsBomb(0);
        boardManager.markAsBomb(1);
        boardManager.markAsBomb(2);
        boardManager.markAsBomb(3);
        assertFalse(boardManager.canMarkMoreBombs());
        boardManager.markAsBomb(0);
        assertTrue(boardManager.canMarkMoreBombs());
    }

    @Test
    public void testScoreComputation() {
        assertEquals(16, boardManager.getScore());
        boardManager.revealTile(0);
        assertEquals(15, boardManager.getScore());
    }

    private void generateBoard() {
        List<MinesweeperTile> tileList = Arrays.asList(new MinesweeperTile[]{
                new MinesweeperTile(9), new MinesweeperTile(1), new MinesweeperTile(1), new MinesweeperTile(1),
                new MinesweeperTile(1), new MinesweeperTile(2), new MinesweeperTile(2), new MinesweeperTile(9),
                new MinesweeperTile(0), new MinesweeperTile(2), new MinesweeperTile(9), new MinesweeperTile(3),
                new MinesweeperTile(0), new MinesweeperTile(2), new MinesweeperTile(9), new MinesweeperTile(2)
        });

        MinesweeperBoard minesweeperBoard = new MinesweeperBoard(tileList, 4, 4);

        boardManager = new BoardManager(minesweeperBoard, 4, true);
    }

    private void solveBoard() {
        boardManager.markAsBomb(0);
        boardManager.markAsBomb(7);
        boardManager.markAsBomb(10);
        boardManager.markAsBomb(14);
    }
}
