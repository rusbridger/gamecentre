package fall2018.csc2017.minesweeper;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MinesweeperTileTest {
    private MinesweeperTile tile;

    @Before
    public void setup() {
        tile = new MinesweeperTile(0);
    }

    @Test
    public void testTileInitiallyHidden() {
        assertTrue(tile.isHidden());
        assertEquals(MinesweeperTile.HIDDEN_TILE, tile.getBackground());
    }

    @Test
    public void testTileRevealsBackgroundWhenNotHidden() {
        tile.setHidden(false);
        assertFalse(tile.isHidden());
        assertEquals(MinesweeperTile.TILES[0], tile.getBackground());
    }

    @Test
    public void testTileFlagged() {
        tile.setFlagged(true);
        assertTrue(tile.isFlagged());
        assertEquals(MinesweeperTile.FLAGGED_TILE, tile.getBackground());
    }

    @Test
    public void testRevealTileForUnflaggedBomb() {
        tile = new MinesweeperTile(MinesweeperBoard.BOMB_VAL);
        tile.setRevealed(true);
        assertEquals(MinesweeperTile.TILES[9], tile.getBackground());
    }

    @Test
    public void testRevealTileForFlaggedBomb() {
        tile = new MinesweeperTile(MinesweeperBoard.BOMB_VAL);
        tile.setFlagged(true);
        tile.setRevealed(true);
        assertEquals(MinesweeperTile.FLAGGED_TILE, tile.getBackground());
    }

    @Test
    public void testRevealTileForFalseFlaggedBomb() {
        tile.setFlagged(true);
        tile.setRevealed(true);
        assertEquals(MinesweeperTile.NOT_BOMB_TILE, tile.getBackground());
    }

    @Test
    public void testRevealTileForHiddenTile() {
        tile.setRevealed(true);
        assertEquals(MinesweeperTile.HIDDEN_TILE, tile.getBackground());
    }

    @Test
    public void testRevealTileForNumberTile() {
        tile.setHidden(false);
        tile.setRevealed(true);
        assertEquals(MinesweeperTile.TILES[0], tile.getBackground());
    }
}
