package fall2018.csc2017.pipes;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Manage a PipesGameBoard, including rotating tiles, checking for a win, and managing taps.
 */
class BoardManager implements Serializable {

    /**
     * Boolean for whether or not the pipesGameBoard is gameOver
     */
    private boolean puzzleSolved;

    /**
     * The PipesGameBoard being managed.
     */
    private PipesGameBoard pipesGameBoard;

    /**
     * Represents the opposite movements of whatever the user has done.
     * By doing so, we only need to store a single number for every undo we make.
     */
    private ArrayDeque<Integer> history;

    /**
     * Represents the number of moves the user has played.
     */
    private int moves;

    /**
     * Represents the lifetime of the PipesGameBoard at it's creation.
     * May be overwritten by other classes, but stored in BoardManager to enable having it saved
     * internally.
     */
    private long lifetime;

    /**
     * Represents the number of start pipes in the board (the number of border pipes).
     */
    private int numBorderPipes = 0;

    /**
     * Represents the first start pipe in the board (by row-major order).
     */
    private int startPipeIndex;

    /**
     * Manage a PipesGameBoard that has been pre-populated.
     *
     * @param PipesGameBoard the PipesGameBoard
     */
    BoardManager(PipesGameBoard pipesGameBoard) {
        this.pipesGameBoard = pipesGameBoard;
        this.history = new ArrayDeque<Integer>();
    }

    /**
     * Return the current PipesGameBoard.
     */
    PipesGameBoard getPipesGameBoard() {
        return pipesGameBoard;
    }

    /**
     * Manage a new shuffled PipesGameBoard of default dimensions.
     */
    BoardManager() {
        this(PipesGameBoard.DEFAULTLEVEL);
    }

    /**
     * Manage a new PipesGameBoard of dimensions specified by the level
     * Precondition: 0 <= level <= 9
     *
     * @param level the level of the PipesGameBoard
     */
    BoardManager(int level) {
        List<PipeTile> tiles = new ArrayList<>();
        final int numTiles = (Math.max(3, level + 2)) * (Math.max(3, level + 2));

        for (int tileNum = 0; tileNum != numTiles; tileNum++) {
            PipeTile newPipeTile = new PipeTile(
                    PipesGameBoard.LEVELS[level][tileNum][0],
                    PipesGameBoard.LEVELS[level][tileNum][1]);
            // if this is a start pipe
            if (newPipeTile.getType() == 1) {
                if (numBorderPipes == 0) startPipeIndex = tileNum; // index of first border pipe
                numBorderPipes++;
            }
            tiles.add(newPipeTile); // add newPipeTile to List
        }
        this.pipesGameBoard = new PipesGameBoard(tiles, level);
        this.history = new ArrayDeque();
        this.moves = 0;
        this.lifetime = 0;
    }

    /**
     * Return whether all of the start pipes are connected to the first one in some way.
     * We build a circuit of all PipeTiles connected to the first start Pipe
     *
     * @return whether all of the start pipes are connected to the first one in some way.
     */
    boolean puzzleSolved() {
        if (this.puzzleSolved) return true; // if already puzzleSolved, don't compute

        boolean hasLeaks = false; // assume there are no leaks in the circuit

        HashSet<Integer> pipeIndicesTravelled = new HashSet<Integer>();
        pipeIndicesTravelled = pipesGameBoard.getConnectedPipes(startPipeIndex, pipeIndicesTravelled);

        int borderPipesTraversed = 0; // integer to count the number of start PipeTiles in the circuit

        // search PipeTiles that have been traversed and add the number of start Pipes are connected
        for (Integer i : pipeIndicesTravelled) {
            // abort the search if some PipeTile in the circuit is not closed (it leaks).
            if (pipesGameBoard.doesLeak(i)) {
                hasLeaks = true;
                break;
            }
            if (pipesGameBoard.getTile(i).getType() == 1) borderPipesTraversed++;
        }
        // true if and only if there are no leaks, and all start PipeTiles are in the circuit
        this.puzzleSolved = (!hasLeaks && borderPipesTraversed == numBorderPipes);
        return this.puzzleSolved;
    }

    /**
     * Return whether the tapped tile is rotatable.
     *
     * @param position the tile to check
     * @return whether the tile at position is surrounded by a blank tile
     */
    boolean isValidTap(int position) {
        int row = position / pipesGameBoard.getNumColumns();
        int col = position % pipesGameBoard.getNumColumns();
        PipeTile pipeTile = pipesGameBoard.getTile(row, col);
        return pipeTile.isRotatable();
    }

    /**
     * Process a touch at position in the PipesGameBoard, rotating a tile as appropriate.
     *
     * @param position the position of the PipeTile
     * @param isUndo   whether or not the move is an undo
     */
    void touchRotate(int position, boolean isUndo) {
        if (!this.puzzleSolved) {
            pipesGameBoard.rotateTile(position, isUndo);
            history.push(position);
            moves += 1;
        }
    }

    /**
     * Attempts to undo the last move. If there are no last moves remaining, does nothing.
     *
     * @return true iff the undo was done; false iff undo can't be done.
     */
    public boolean undo() {
        if (history.isEmpty() || this.puzzleSolved) {
            return false;
        } else {
            touchRotate(history.pop(), true);
            history.pop(); // pop the undo move itself
            // undos are a move, or at least should be penalized. No moves deducted.
            return true;
        }
    }

    /**
     * Returns the current score of the PipesGameBoard. The formula uses the max number of
     * necessary moves to compute a score. If the player rotates every PipeTile more a full circle
     * or more, the score is 0 or negative.
     *
     * @return current score of the PipesGameBoard
     */
    public int getScore() {
        /*  deduct moves from the max number of necessary moves to get the score. If the player
            rotates every PipeTile a full circle or more, the score is 0 or negative.
        */
        return this.pipesGameBoard.getNumOfPlayable() * 3 - moves;
    }

    /**
     * Returns the lifetime of this PipesGameBoard at creation.
     *
     * @return the lifetime of this PipesGameBoard at creation
     */
    public long getLifetime() {
        return lifetime;
    }

    /**
     * Sets the new lifetime of this PipesGameBoard for when it is to be saved.
     */
    public void setLifetime(long lifetime) {
        this.lifetime = lifetime;
    }
}
