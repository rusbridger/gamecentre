package fall2018.csc2017.pipes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import fall2018.csc2017.CustomAdapter;
import fall2018.csc2017.db.Games;
import fall2018.csc2017.db.GamesaveController;
import fall2018.csc2017.db.ScoreController;
import fall2018.csc2017.db.User;

import fall2018.csc2017.R;

/**
 * The game activity.
 */
public class GameActivity extends AppCompatActivity implements Observer {
    /**
     * The buttons to display.
     */
    private ArrayList<Button> tileButtons;

    BoardManager boardManager;

    // Grid View
    private GestureDetectGridView gridView;
    private static int columnWidth, columnHeight;

    private long startup_time;

    /**
     * Set up the background image for each button based on the master list
     * of positions, and then call the adapter to set the view.
     */
    // Display
    public void display() {
        updateTileButtons();
        gridView.setAdapter(new CustomAdapter(tileButtons, columnWidth, columnHeight));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        boardManager = (BoardManager) getIntent().getSerializableExtra("boardManager");
        // this should NEVER happen assuming PipesStartingActivity does it's job.
        if (boardManager == null) {
            boardManager = new BoardManager();
        }

        createTileButtons(this);
        setContentView(R.layout.activity_pipes_main);

        // Add View to activity
        gridView = findViewById(R.id.grid);
        final int num_rows = boardManager.getPipesGameBoard().numRows;
        final int num_cols = boardManager.getPipesGameBoard().numColumns;
        gridView.setNumColumns(num_cols);
        gridView.setBoardManager(boardManager);
        boardManager.getPipesGameBoard().addObserver(this);
        // Observer sets up desired dimensions as well as calls our display function
        gridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        gridView.getViewTreeObserver().removeOnGlobalLayoutListener(
                                this);
                        int displayWidth = gridView.getMeasuredWidth();
                        int displayHeight = gridView.getMeasuredHeight();

                        int displaySize = Math.min(displayHeight, displayWidth);

                        columnWidth = displaySize / num_cols;
                        columnHeight = displaySize / num_rows;

                        display();
                    }
                });
        addUndoButtonListener();
        startup_time = System.nanoTime();

        startInfoUpdateThread();
    }

    /**
     * Starts a thread to update info on time spent and score.
     */
    private void startInfoUpdateThread() {
        // Start a new thread
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        // Update once every second
                        Thread.sleep(1000);
                        runOnUiThread(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!boardManager.puzzleSolved()) {
                                            // Update the fields
                                            TextView time = findViewById(R.id.TimerText);
                                            long timeChanged = (long) (
                                                    (boardManager.getLifetime() + System.nanoTime()
                                                            - startup_time) * 1e-9);
                                            time.setText("TIME: " + timeChanged);

                                            TextView moves = findViewById(R.id.ScoreText);
                                            moves.setText("SCORE: " + boardManager.getScore());
                                        }
                                    }
                                }
                        );
                    }
                } catch (InterruptedException e) {

                }
            }
        };

        thread.start();
    }

    /**
     * Create the buttons for displaying the tiles.
     *
     * @param context the context
     */
    private void createTileButtons(Context context) {
        PipesGameBoard pipesGameBoard = boardManager.getPipesGameBoard();
        tileButtons = new ArrayList<>();
        for (int row = 0; row != pipesGameBoard.numRows; row++) {
            for (int col = 0; col != pipesGameBoard.numColumns; col++) {
                Button tmp = new Button(context);
                tmp.setBackgroundResource(pipesGameBoard.getTile(row, col).getBackground());
                this.tileButtons.add(tmp);
            }
        }
    }

    /**
     * Update the backgrounds on the buttons to match the tiles.
     */
    private void updateTileButtons() {
        PipesGameBoard pipesGameBoard = boardManager.getPipesGameBoard();
        int nextPos = 0;
        for (Button b : tileButtons) {
            int row = nextPos / pipesGameBoard.numRows;
            int col = nextPos % pipesGameBoard.numColumns;
            b.setBackgroundResource(pipesGameBoard.getTile(row, col).getBackground());
            nextPos++;
        }
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
        try {
            saveGame();
        } catch (IOException e) {
            Log.e("pipes", "Error during autosave", e);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (boardManager.puzzleSolved()) {
            Toast.makeText(this.getApplicationContext(), "YOU WIN!", Toast.LENGTH_SHORT).show();
            winGame();
        }
        display();
    }

    /**
     * Saves the current game state to the database.
     *
     * @throws IOException
     */
    private void saveGame() throws IOException {
        long timeChanged = System.nanoTime() - startup_time;

        boardManager.setLifetime(boardManager
                .getLifetime() + timeChanged);

        GamesaveController.createGamesave(User.getSignedInUser().getUsername(),
                Games.PIPES.getValue(), boardManager);

    }

    /**
     * Adds listener for UndoButton.
     */
    private void addUndoButtonListener() {
        Button undoButton = findViewById(R.id.UndoButton);
        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boardManager.puzzleSolved()) {
                    Toast.makeText(GameActivity.this, "You already won!",
                            Toast.LENGTH_SHORT).show();
                } else if (!boardManager.undo()) { // if the user ran out of undos
                    Toast.makeText(GameActivity.this, "No moves to undo ¯\\_(ツ)_/¯",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Saves the score, then exits the game.
     */
    private void winGame() {
        BoardManager gameBoard = boardManager;
        long finalTime = System.nanoTime() + gameBoard.getLifetime() - startup_time;
        ScoreController.createScore(User.getSignedInUser().getUsername(),
                Games.PIPES.getValue(), finalTime, gameBoard.getScore());
    }
}
