package fall2018.csc2017.db;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DatabaseController {
    private static AppDatabase mainDB = null;

    /**
     * Initializes the database. This should **ONLY** be run at application startup.
     *
     * @param context
     */
    public static void init(Context context) {
        mainDB = Room.databaseBuilder(context, AppDatabase.class, "main").allowMainThreadQueries().build();
    }

    /*
     * Provides the database for other controllers to use.
     */
    static AppDatabase provideDatabase() {
        return mainDB;
    }

    /**
     * Terminates the database.
     * Only use this in testing or when closing the app.
     */
    public static void kill() {
        if (mainDB != null) {
            mainDB.close();
            mainDB = null;
        }
    }

    /**
     * Resets all database tables. Only to be used in unit tests.
     */
    public static void reset() {
        mainDB.clearAllTables();
    }
}
