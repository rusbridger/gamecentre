package fall2018.csc2017.db;

/**
 * Wrapper class to avoid having to expose the database to the rest of the application.
 */
public class ScoreController {

    /**
     * Creates a new score without having to manually use the constructor.
     */
    public static void createScore(String username, int gameId, long timeTaken, int score) {
        createScore(new Score(username, gameId, timeTaken, score));
    }

    /**
     * Returns all the scores for a specified game, in descending order.
     * The entries are sorted by the score, which is defined differently for each game.
     *
     * @return the sorted array of scores
     */
    public static Score[] sortByScore(int gameId) {
        ScoreDao sDao = DatabaseController.provideDatabase().scoreDao();
        return sDao.sortByScore(gameId);
    }

    /**
     * Returns all the scores for a specified game, in ascending order of time taken.
     *
     * @return the sorted array of scores
     */
    public static Score[] sortByTimeTaken(int gameId) {
        ScoreDao sDao = DatabaseController.provideDatabase().scoreDao();
        return sDao.sortByTimeTaken(gameId);
    }

    /**
     * Returns all the scores of a specified user for a specified game, in descending order.
     * Entries sorted by the actual score.
     *
     * @return the sorted array of scores from a given username.
     */
    public static Score[] sortByScore(int gameId, String username) {
        ScoreDao sDao = DatabaseController.provideDatabase().scoreDao();
        return sDao.sortByScore(gameId, username);
    }

    /**
     * Returns all the scores of a specified user for a specified game, in ascending order.
     * Entries sorted by the time taken.
     *
     * @return the sorted array of scores from a given username.
     */
    public static Score[] sortByTimeTaken(int gameId, String username) {
        ScoreDao sDao = DatabaseController.provideDatabase().scoreDao();
        return sDao.sortByTimeTaken(gameId, username);
    }

    /*
     * Creates a new score from the specified score.
     * @param score
     */
    private static void createScore(Score score) {
        ScoreDao sDao = DatabaseController.provideDatabase().scoreDao();
        sDao.createScore(score);
    }
}
